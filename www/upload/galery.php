<?php 
	session_start();
	/**************************************
    * Create databases and                *
    * open connections                    *
    **************************************/
 
    // Create (connect to) SQLite database in file
    $file_db = new PDO('sqlite:photos.sqlite3');
    // Set errormode to exceptions
    $file_db->setAttribute(PDO::ATTR_ERRMODE, 
                            PDO::ERRMODE_EXCEPTION);
 
    /**************************************
    * Create tables                       *
    **************************************/
 
    // Create table messages
    $file_db->exec("CREATE TABLE IF NOT EXISTS photos (
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    author text,
                    type text,
                    content BLOB)");

	if (@$_POST['login']){
		$_SESSION['login'] = @$_POST['login'];
	};

	if (@$_SESSION['login'] && @$_POST['logout']){
		session_destroy();
		header("Location: galery.php");
	}

	if (@$_SESSION['login'] && @$_POST['delete']){
		try {
			// Prepare INSERT statement to SQLite3 file db
			$stmt = $file_db->prepare("DELETE FROM photos WHERE author like :author");

			// Bind parameters to statement variables
			$stmt->bindParam(':author',  $_SESSION['login']);

			$stmt->execute();
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
	}

	if (@$_FILES['image']){
		if (!@$_SESSION['login']){
			die("Musisz być zalogowany, aby dodawać obrazki!");
		}
		try {
		/**************************************
		* Play with databases and tables      *
		**************************************/

		// Prepare INSERT statement to SQLite3 file db
		$insert = "INSERT INTO photos (author, content, type) 
		            VALUES (:author, :content, :type)";
		$stmt = $file_db->prepare($insert);

		// Bind parameters to statement variables
		$author = $_SESSION['login'];
		$content = file_get_contents($_FILES['image']['tmp_name']);
		$stmt->bindParam(':author', $author);
		$stmt->bindParam(':content', $content);
		$stmt->bindParam(':type', $_FILES['image']['type']);

		$stmt->execute();
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Photo Galery</title>
	<link rel="stylesheet" href="">
</head>
<body>
<?php
if (@$_GET['framebusting']){
	echo '
	<script>
		if (top !== self) {
			top.location = self.location;
		}
	</script>';
}
?>
<div style="text-align: center; margin-left: auto; margin-right: auto">

<?php
if (@$_SESSION['login']){
	echo "	
	<h2 id='login'>Strefa dla zalogowanych - użytkownik <em>'".$_SESSION['login']."'</em></h2>
	";
	if (@$_GET['framebusting']){
		echo "<form method='post' action='galery.php?framebusting=1' enctype='multipart/form-data'>";
	}
	else echo "
		<form method='post' action='galery.php' enctype='multipart/form-data'>";

	echo "
		<input type='file' name='image'></input>
		<input type='submit' value='Dodaj nowy obrazek'></input>
	</form>

	<br />

	<form method='post' action='galery.php'>
		<input type='submit' name='delete' value='Remove my photos'>
	</form>	

	<br />

	<form method='post' action='galery.php'>
		<input type='submit' name='logout' value='logout'>
	</form>
	";
}
else {
	echo "
		<form method='post' action='galery.php'>
			<input type='text' name='login' placeholder='login'>
			<input type='password' name='password' placeholder='password (whatever)'>
			<input type='submit' value='Zaloguj'>
		</form>
	";
}
?>
	<br />
	<hr />
	<h2 id='galery'>Galeria</h2>
	<?php
		try {
			// Select all data from memory db messages table 
			$result = $file_db->query('SELECT * FROM photos ORDER BY id DESC');

			foreach($result as $row) {
				$id = $row['id'];
				$author = $row['author'];
			  echo "<img src='showimage.php?id=$id' alt='Author: $author' />
			  <br /><strong>Author: $author</strong><br /><br />";
			}
		}
		catch(PDOException $e) {
			// Print PDOException message
			echo $e->getMessage();
		}

	?>
	<hr />
</div>
<br />
</body>
</html>